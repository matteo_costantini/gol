package it.uniba.gol;

import static org.junit.Assert.*;
import org.junit.Test;

public class CellTest {

	@Test
	public void cellShouldBeAlive() throws CustomLifeException {
		Cell cell = new Cell(0,0,true);
		assertTrue(cell.isAlive());
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseException() throws CustomLifeException {
		Cell cell = new Cell(-1,0,true);
	}
	
	@Test
	public void cellShouldBeDead() throws CustomLifeException {
		Cell cell = new Cell(0,0,true);
		cell.setAlive(false);
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void cellShouldReturnX() throws CustomLifeException {
		Cell cell = new Cell(5,0,true);
		assertEquals(5,cell.getX());
	}
	
	@Test
	public void cellShouldReturnY() throws CustomLifeException {
		Cell cell = new Cell(0,5,true);
		assertEquals(5,cell.getY());
	}
	
	@Test
	public void cellShouldHaveAliveNeighbors() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,true);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, ha 4 vicini vivi
		assertEquals(4,cells[1][1].getNumberOfAliveNeighbords());
	}
	
	@Test
	public void cellShouldSurviveThreeNeighbors() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, ha 3 vicini vivi
		assertTrue(cells[1][1].willSurvive());
	}
	
	@Test
	public void cellShouldNotSurviveFourNeighbors() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, ha 4 vicini vivi
		assertFalse(cells[1][1].willSurvive());
	}
	
	@Test
	public void cellShouldNotSurviveDeadCell() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, che � morta
		assertFalse(cells[1][1].willSurvive());
	}
	
	@Test
	public void cellShoulDieFourNeighbors() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, ha 4 vicini vivi
		assertTrue(cells[1][1].willDie());
	}
	
	@Test
	public void cellShouldNotDieThreeNeighbors() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, ha 3 vicini vivi
		assertTrue(cells[1][1].willDie());
	}
	
	@Test
	public void cellShouldNotDieDeadCell() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, che � morta
		assertFalse(cells[1][1].willDie());
	}
	
	@Test
	public void cellShouldReviveThreeNeighbors() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, che � morta e ha 3 vicini vivi
		assertTrue(cells[1][1].willRevive());
	}
	
	@Test
	public void cellShouldNotReviveThreeNeighbors() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		// Faccio assert sulla cella centrale, che � viva e ha 3 vicini vivi
		assertFalse(cells[1][1].willRevive());
	}
	
	@Test
	public void cellShouldNotBeNeighbor() throws CustomLifeException {
		Cell cell1 = new Cell(0,0,true);
		Cell cell2 = new Cell(2,2,true);
		
		// Assert
		assertFalse(cell1.isNeighboard(cell2));
	}
}
